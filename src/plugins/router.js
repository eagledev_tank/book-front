import {createRouter, createWebHistory} from 'vue-router'

const routes = [
    {
        path: '/',
        component: () => import('@/pages/HomePage.vue')
    },
    {
        path: '/categories/:id',
        component: () => import('@/pages/HomePage.vue')
    },
    {
        path: '/books/:page',
        component: () => import('@/pages/HomePage.vue')
    },
    {
        path: '/book-info/:bookId',
        component: () => import('@/pages/BookInfoPage.vue')
    },
    {
        path: '/login',
        component: () => import('@/pages/LoginPage.vue')
    },
    {
        path: '/add-book',
        component: () => import('@/pages/AddBook.vue')
    },
    {
        path: '/sign_up',
        component: () => import('@/pages/SignUpPage.vue')
    }
]

export default createRouter({
    history: createWebHistory(),
    routes
})
