import axios from "./axios";

export default {
    actions: {
        uploadPicture(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post('http://localhost:8888/api/media_objects',
                        data,
                        {
                            headers: {
                                'Content-Type': 'multipart/form-data'
                            }
                        })
                    .then((response) => {
                        console.log(response)
                        context.commit('updatePictureId', response.data['@id'])
                        console.log('rasm yuklandi')
                        resolve()
                    })
                    .catch(() => {
                        console.log('rasmni yuklamading')
                        reject()
                    })
            })
        }
    },
    mutations: {
        updatePictureId(state, pictureId) {
            state.pictureId = pictureId;
        }
    },
    state: {
        pictureId:''
    },
    getters: {
        getPictureId(state) {
            return state.pictureId;
        }
    }
}
