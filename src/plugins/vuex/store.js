import {createStore} from "vuex";
import user from "@/plugins/vuex/user";
import book from "@/plugins/vuex/book";
import category from "@/plugins/vuex/category";
import picture_upload from "@/plugins/vuex/picture_upload";

export default  createStore({
    modules: {
        book,
        category,
        picture_upload,
        user
    }
})