import axios from "axios";

export default {
    actions: {
        fetchUserToken(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post('http://localhost:8888/api/users/auth', data)
                    .then((response) => {
                        console.log('muvaffaqiyatli, then() ishladi')
                        console.log(response.data)
                        context.commit('updateToken', response.data.token)
                        resolve()
                    })
                    .catch(() => {
                        console.log('muvaffaqiyatsiz, catch() ishladi')
                        reject()
                    })
                    .finally( () => {
                        console.log('oxirgi bolib finally() ishladi')
                    })
            })
        },
        createUser(context, data){
            return new Promise((resolve, reject) =>{
                axios
                    .post('http://localhost:8888/api/users/create', data)
                    .then(()=>{
                        console.log('user yaratildi')
                        resolve()
                    })
                    .catch(() => {
                        console.log('muvaffaqiyatsiz, catch() ishladi')
                        reject()
                    })

            })
        }
    },
    mutations: {
        updateToken(state, token) {
            state.token = token
            localStorage.setItem('token', token)
        }
    },
    state: {
        token: localStorage.getItem('token')
    },
    getters: {
        getToken(state) {
            return state.token
        }
    }
}
